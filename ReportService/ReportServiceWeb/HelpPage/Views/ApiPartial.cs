﻿using System;
using HIT.Web.ReportServiceWeb.HelpPage.Models;

namespace HIT.Web.ReportServiceWeb.HelpPage
{
    partial class Api
    {
        public HelpPageApiModel Model { get; set; }

        public string HomePageLink { get; set; }
    }
}