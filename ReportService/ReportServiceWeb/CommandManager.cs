﻿using HIT.Web.ReportContracts.DataContracts;
using HIT.Web.ReportServiceDb;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace HIT.Web.ReportServiceWeb
{
    internal class CommandManager: IDisposable
    {
        public ReportDbManager DbManager { get; private set; }
        public CommandManager(ReportDbManager dm)
        {
            DbManager = dm;
        }
        public IEnumerable<String> GetDates()
        {
            Debug.Assert(DbManager != null);
            return DbManager.GetDates();
        }
        public IEnumerable<WebsiteStatistic> GetTop(string date, int count)
        {
            Debug.Assert(DbManager != null);
            var x = DbManager.GetTop(date, count);
            return x.Result;
        }
        public void Dispose()
        {
            if (DbManager != null)
            {
                DbManager.Dispose();
            }
        }
    }
    internal static class CommandManagerInstance
    {
        private static volatile CommandManager _cm;
        private static object _instanceSyncObj = new object();
        public static CommandManager CommandManager
        {
            get
            {
                if (_cm == null)
                {
                    throw new Exception("Call create method first.");
                }
                lock (_instanceSyncObj)
                {
                    return _cm;
                }
            }
        }

        public static void Create(ReportDbManager dm)
        {
            if (_cm == null)
            {
                lock (_instanceSyncObj)
                {
                    if (_cm == null)
                    {
                        _cm = new CommandManager(dm);
                    }
                }
            } 
        }
        public static void Dispose()
        {
            if (_cm == null)
            {
                throw new Exception("Call create method first.");
            }
            lock (_instanceSyncObj)
            {
                _cm.Dispose();
            }
        }
    }
    public class NoCacheResult : IHttpActionResult
    {
        public IHttpActionResult InnerResult { get; private set; }
        public NoCacheResult(IHttpActionResult har)
        {
            InnerResult = har;
        }
        public async Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            var response = await InnerResult.ExecuteAsync(cancellationToken);
            response.Headers.CacheControl = new System.Net.Http.Headers.CacheControlHeaderValue()
            {
                NoCache = true,
                NoStore = true,
            };
            return response;
        }
    }
    public static class HttpActionResultExtension
    {
        public static NoCacheResult NoCache(this IHttpActionResult har)
        {
            return new NoCacheResult(har);
        }
    }
}
