﻿using HIT.Web.ReportContracts.DataContracts;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;

namespace HIT.Web.ReportServiceWeb
{
    /// <summary>
    /// Get website statistic data
    /// </summary>
    [RoutePrefix("api/report/websiterank")]
    public class WebsiteRankController: ApiController
    {
        /// <summary>
        /// Get all dates
        /// </summary>
        /// <returns></returns>
        [Route("dates")]
        [HttpGet]
        [ResponseType(typeof(IEnumerable<string>))]
        public IHttpActionResult GetAllDates()
        {
            var cm = CommandManagerInstance.CommandManager;
            return Ok<string[]>(cm.GetDates().ToArray()).NoCache();
        }
        /// <summary>
        /// Get top site visit numbers per weekday
        /// </summary>
        /// <param name="date"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        [Route("top")]
        [HttpGet]
        [ResponseType(typeof(IEnumerable<WebsiteStatistic>))]
        public IHttpActionResult GetTop(string date, int count)
        {
            var cm = CommandManagerInstance.CommandManager;
            return Ok<WebsiteStatistic[]>(cm.GetTop(date, count).ToArray()).NoCache();
        }
    }
}
