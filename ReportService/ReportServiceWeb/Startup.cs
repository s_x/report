﻿using HIT.Web.ReportServiceDb;
using HIT.Web.ReportServiceWeb.HelpPage;
using Microsoft.Owin.Hosting;
using Owin;
using System;
using System.Web.Http;
using System.Web.Http.Cors;

namespace HIT.Web.ReportServiceWeb
{
    internal class Startup
    {
        public void Configuration(IAppBuilder appBuilder)
        {
            HttpConfiguration config = new HttpConfiguration();
            var cors = new EnableCorsAttribute("*", "*", "*");
            config.EnableCors(cors);
            config.MapHttpAttributeRoutes();
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/report/{controller}/{action}/{id}",
                defaults: new { action = RouteParameter.Optional, id = RouteParameter.Optional }
            );
            HelpPageConfig.Register(config);
            appBuilder.UseWebApi(config);
        }
    }
    public class ReportApiWebHost: IDisposable
    {
        private IDisposable _webApp;
        public ReportApiWebHost(string baseAddress, ReportDbManager dm)
        {
            CommandManagerInstance.Create(dm);
            _webApp = WebApp.Start<Startup>(url: baseAddress);
        }
        public void Dispose()
        {
            _webApp.Dispose();
            CommandManagerInstance.Dispose();
        }
    }
    [RoutePrefix("api/report/help")]
    public class HelpController : HelpControllerBase
    { }
}
