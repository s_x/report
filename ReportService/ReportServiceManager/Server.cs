﻿using HIT.Web.ReportServiceDb;
using HIT.Web.ReportServiceWeb;

namespace HIT.Web.ReportServiceManager
{
    public class Server
    {
        private static ReportApiWebHost _reportApiWebHost;
        private static object _instanceSyncObj = new object();
        public static void Start()
        {
            lock (_instanceSyncObj)
            {
                ReportDbManager dm = new ReportDbManager();
                string baseAddress = "http://+:10101/";
                _reportApiWebHost = new ReportApiWebHost(baseAddress, dm);
            }
        }
        public static void Stop()
        {
            lock (_instanceSyncObj)
            {
                if (_reportApiWebHost != null)
                {
                    _reportApiWebHost.Dispose();
                    _reportApiWebHost = null;
                }
            }
        }
    }
}
