﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIT.Web.ReportContracts.DataContracts
{
    public class WebsiteStatistic
    {
        public String Name { get; set; }
        public Int32 Visits { get; set; }
        public WebsiteStatistic()
        { }
        public WebsiteStatistic(String name, Int32 visits)
        {
            Name = name;
            Visits = visits;
        }
    }
}
