﻿namespace HIT.Web.ReportServiceManagerHost
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ReportServiceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.ReportServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // ReportServiceProcessInstaller
            // 
            this.ReportServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.ReportServiceProcessInstaller.Password = null;
            this.ReportServiceProcessInstaller.Username = null;
            // 
            // ReportServiceInstaller
            // 
            this.ReportServiceInstaller.ServiceName = "ReportService Manager";
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.ReportServiceProcessInstaller,
            this.ReportServiceInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller ReportServiceProcessInstaller;
        private System.ServiceProcess.ServiceInstaller ReportServiceInstaller;
    }
}