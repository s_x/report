﻿using HIT.Web.ReportServiceManager;
using System;

namespace HIT.Web.ReportServiceManagerHost
{
    internal static class ServiceConsole
    {
        public static void Run(string[] args)
        {
            Console.Title = "Report Service";
            Server.Start();
            Console.WriteLine("Service started");
            Console.ReadLine();
            Server.Stop();
            Console.WriteLine("Service stopped");
        }
    }
}
