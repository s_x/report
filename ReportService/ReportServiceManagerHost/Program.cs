﻿using System;
using System.ServiceProcess;

namespace HIT.Web.ReportServiceManagerHost
{
    class Program
    {
        static void Main(string[] args)
        {
            if (Environment.UserInteractive)
            {
                ServiceConsole.Run(args);
            }
            else
            {
                ServiceBase[] services = new ServiceBase[] { new ReportServiceManagerWindowsService() };
                ServiceBase.Run(services);
            }
        }
    }
}
