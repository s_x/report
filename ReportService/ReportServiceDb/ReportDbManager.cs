﻿using HIT.Web.ReportContracts.DataContracts;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace HIT.Web.ReportServiceDb
{
    public class ReportDbManager: IDisposable
    {
        public ReportDbManager()
        {
            //Populate();
        }
        public void Populate()
        {
            using (var db = new WebsitesContext())
            {
                var r = new Random();
                var maxhits = 100000;
                var maxweeks = 52;
                var websites = new string[] {
                    "google", "amazon", "microsoft", "oracle", "sap",
                    "yahoo", "facebook", "cisco", "ericsson", "bell",
                    "rogers",  "td", "rbc", "bmo", "manulife",
                    "sunlife", "cbc", "aol", "disney", "hit"
                };
                var ws = new List<WebsiteVisit>();
                for (int i = 0; i < websites.Length; i++)
                {
                    for (int j = 1; j <= maxweeks; j++)
                    {
                        ws.Add(new WebsiteVisit() { Name = websites[i], Date = "Week " + j, Visits = r.Next(maxhits) });
                    }
                }                   
                ws.ForEach(s => { db.WebsiteVisits.Add(s); });
                db.SaveChanges();
            }
        }
        public IEnumerable<string> GetDates()
        {
            using (var db = new WebsitesContext())
            {
                var query = (from x in db.WebsiteVisits
                            orderby x.Date                            
                            select x.Date).Distinct();
                return query.ToList().OrderBy(y => y, new Fn.AlphaNumComparatorFast());
            }
            //for(int i = 0; i < 10; i++)
            //{
            //    yield return "Date " + i;
            //}
        }
        public async Task<IEnumerable<WebsiteStatistic>> GetTop(string date, int count)
        {
            using (var db = new WebsitesContext())
            {
                var query = await (from x in db.WebsiteVisits
                            orderby x.Visits descending
                            where x.Date == date
                            select x).ToListAsync();
                int i = 0;
                List<WebsiteStatistic> ws = new List<WebsiteStatistic>();
                foreach (var item in query)
                {
                    if (++i <= count)
                    {
                        ws.Add(new WebsiteStatistic(item.Name, item.Visits));
                    }
                }
                return ws.OrderBy(y => y.Visits);
            }
            //for (int i = 0; i < count; i++)
            //{
            //    yield return new WebsiteStatistic("website " + i, i);
            //}
        }
        public void Dispose()
        {
        }
    }
    public static class Fn
    {
        public class AlphaNumComparatorFast: IComparer<string>
        {
            public int Compare(string s1, string s2)
            {
                if (s1 == null && s2 == null)
                {
                    return 0;
                }
                if (s1 == null)
                {
                    return -1;
                }
                if (s2 == null)
                {
                    return 1;
                }

                int len1 = s1.Length;
                int len2 = s2.Length;
                int marker1 = 0;
                int marker2 = 0;

                while (marker1 < len1 && marker2 < len2)
                {
                    char ch1 = s1[marker1];
                    char ch2 = s2[marker2];

                    char[] space1 = new char[len1];
                    int loc1 = 0;
                    char[] space2 = new char[len2];
                    int loc2 = 0;

                    do
                    {
                        space1[loc1++] = ch1;
                        marker1++;

                        if (marker1 < len1)
                        {
                            ch1 = s1[marker1];
                        }
                        else
                        {
                            break;
                        }
                    } while (char.IsDigit(ch1) == char.IsDigit(space1[0]));

                    do
                    {
                        space2[loc2++] = ch2;
                        marker2++;

                        if (marker2 < len2)
                        {
                            ch2 = s2[marker2];
                        }
                        else
                        {
                            break;
                        }
                    } while (char.IsDigit(ch2) == char.IsDigit(space2[0]));

                    string str1 = new string(space1);
                    string str2 = new string(space2);

                    int result;

                    if (char.IsDigit(space1[0]) && char.IsDigit(space2[0]))
                    {
                        int thisNumericChunk = int.Parse(str1);
                        int thatNumericChunk = int.Parse(str2);
                        result = thisNumericChunk - thatNumericChunk;
                    }
                    else
                    {
                        result = String.Compare(str1, str2, StringComparison.CurrentCultureIgnoreCase);
                    }

                    if (result != 0)
                    {
                        return result;
                    }
                }
                return len1 - len2;
            }
        }

        public class NaturalStringComparer: IComparer<string>
        {
            public int Compare(string x, string y)
            {
                if (x == null && y == null)
                {
                    return 0;
                }
                if (x == null)
                {
                    return -1;
                }
                if (y == null)
                {
                    return 1;
                }

                string[] xParts = Regex.Split(x, "(\\d+)");
                string[] yParts = Regex.Split(y, "(\\d+)");
                int result = 0;
                for (int i = 0; result == 0 && i < xParts.Length; i++)
                {
                    if (yParts.Length <= i)
                    {
                        result = 1;
                    }

                    int xNum = -1;
                    int yNum = -1;
                    if (int.TryParse(xParts[i], out xNum))
                    {
                        if (int.TryParse(yParts[i], out yNum))
                        {
                            result = xNum - yNum;
                        }
                        else
                        {
                            result = 1;
                        }
                    }
                    else
                    {
                        result = String.Compare(xParts[i], yParts[i], StringComparison.CurrentCultureIgnoreCase);
                    }
                }
                return result;
            }
        }
    }
}
