namespace HIT.Web.ReportServiceDb
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class WebsitesContext : DbContext
    {
        public WebsitesContext()
            : base("name=hitdb")
        {
        }

        public virtual DbSet<WebsiteVisit> WebsiteVisits { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
