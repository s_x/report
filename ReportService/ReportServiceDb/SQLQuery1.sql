﻿CREATE TABLE [dbo].[WebsiteVisit] (
    [Name]   NVARCHAR (50) NOT NULL,
    [Date]   NVARCHAR(50)  NOT NULL,
    [Visits] INT           NOT NULL,
    CONSTRAINT [PK_Table] PRIMARY KEY CLUSTERED ([Name] ASC, [Date] ASC)
);
INSERT INTO [dbo].[WebsiteVisit] ([Name],[Date],[Visits]) 
VALUES ('Week 1', 'google', 100);