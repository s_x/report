# README #

This is to demonstrate online report service using ASP.net web API(OWIN) and SQL server(Entity Framework), Azure cloud service deployment and report client using Angular 2 framework. Use sample data for querying website ranking.

### How do I get set up? ###

* Summary of set up
Clone the repository and open server/client solutions separately in Visual Studio 2015.
* Configuration
* Dependencies
- ReportService: OWIN, Entity Framework
- ReportClient: Angular 2, node.js, npm
* Database configuration
* How to run tests
- client: http://reportclient.azurewebsites.net/
- restful api: http://reportservice.cloudapp.net:10101/api/report/help
* Deployment instructions
- ReportService: follow Azure cloud service guideline
- ReportClient: follow Azure app service guideline

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Contact Samuel Xue via samuelxue86@yahoo.com or erlangprogram@gmail.com