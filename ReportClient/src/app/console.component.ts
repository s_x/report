﻿import {Component, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {AboutDialog} from './about.component';
import './rxjs_operators';
@Component({
    selector: 'main-app',
    template: `
        <about-dialog></about-dialog>
        <nav class="navbar navbar-default navbar-static-top" style="margin-bottom:0px">
            <div class="container-fluid">
                <div class="navbar-header header-split">
                    <button type="button" class="navbar-toggle" (click)="toggle()">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <img src="./src/assets/img/favicon.ico" style="margin:10px 10px 10px 0px"/>
                    <a class="navbar-brand" style="cursor: default"></a>
                </div>
                <div class="navbar-collapse" [class.collapse]="isCollapsed">
                    <ul class="nav navbar-nav">
                        <li [class.active]="isActive('dashboard')" (click)="onSelectMenu('dashboard')"><a [routerLink]="['/dashboard']">Dashboard</a></li>
                    </ul>
                    <a class="about" (click)="onAbout()">About</a>
                </div>
            </div>
        </nav>
        <div class="container-fluid" style="margin:0px; padding:0px">
            <router-outlet></router-outlet>
        </div>
      `,
    styles: [`
    `],
    providers: [],
})
export class ConsoleComponent {
    @ViewChild(AboutDialog) aboutDialog: AboutDialog;
    public isCollapsed: boolean = true;
    constructor(private router: Router) { }
    isActive(routePath: string): boolean {
        if (this.router.url == "/" && routePath == "dashboard") {
            return true;
        } else {
            return this.router.url.indexOf(routePath) != -1;
        }
    }
    toggle() {
        this.isCollapsed = !this.isCollapsed;
    }
    onSelectMenu(item: string) {
        this.isCollapsed = true;
    }
    onAbout() {
        this.aboutDialog.show();
    }
}