﻿import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {enableProdMode} from '@angular/core';
import {ConsoleModule} from './console.module';
enableProdMode();
const platform = platformBrowserDynamic();
platform.bootstrapModule(ConsoleModule);