﻿//import 'rxjs/Rx'; // Add all RxJS statics & operators to Observable

// Statics
import 'rxjs/add/observable/throw';
import 'rxjs/add/observable/timer';

// Operators
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/timeout';