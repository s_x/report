﻿import {Component} from '@angular/core';
@Component({
    selector: 'about-dialog',
    templateUrl: './src/app/about.component.html',
})
export class AboutDialog {
    title: string;
    author: string;
    version: string;
    isVisible: boolean;
    show() {
        this.title = "Website Rank Report";
        this.author = "Samuel Xue";
        this.version = "UI Version: 1.0.0 - Build 1 (03-31)";
        this.isVisible = true;
    }
    hide() {
        this.isVisible = false;
    }
}