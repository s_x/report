﻿import {Component, ViewChild} from '@angular/core';
import {OnInit, OnDestroy} from '@angular/core';
import {Observable} from 'rxjs/Rx';
import {Subscription} from 'rxjs/Subscription';
import {HttpService, DialogType, WebsiteStatistic} from './http.service';
import {InformationDialog} from './information.component';
@Component({
    selector: 'snapshot-dashboard',
    templateUrl: './src/app/dashboard.component.html',
    styleUrls: ['./src/app/dashboard.component.css'],
})
export class DashboardComponent {
    @ViewChild(InformationDialog) informationDialog: InformationDialog;
    allDates: Array<string>;
    selectedDate: string;
    topVisitedWebsites: Array<WebsiteStatistic>;
    retrievedDates: boolean;
    retrievedTop: boolean;
    constructor(private httpService: HttpService) {
        this.allDates = [];
        this.topVisitedWebsites = [];
        this.retrievedDates = false;
        this.retrievedTop = true;
        this.queryAllDates();
    } 
    queryAllDates() {
        this.httpService.getWebsiteRankDates()
            .subscribe(data => {
                this.retrievedDates = true;
                this.allDates = data;
            },
            error => { this.informationDialog.show(DialogType.Error, error) },
            () => console.log('queryAllDates completed.'));
    }
    onSelectDate(date: string) {
        this.retrievedTop = false;
        this.selectedDate = date;
        this.httpService.getWebsiteRankTop(this.selectedDate, 7)
            .subscribe(data => {
                this.retrievedTop = true;
                this.topVisitedWebsites = data;
            },
            error => { this.informationDialog.show(DialogType.Error, error) },
            () => console.log('getWebsiteRankTop completed.'));
    }
}