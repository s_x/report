﻿import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HttpModule} from '@angular/http';
import {ConsoleComponent} from './console.component';
import {DashboardComponent} from './dashboard.component';
import {appRouterProviders} from './console.routes';
import {HttpService} from './http.service';
import {AboutDialog} from './about.component';
import {InformationDialog} from './information.component';
@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        appRouterProviders,
    ],
    declarations: [
        ConsoleComponent,
        DashboardComponent,
        AboutDialog,
        InformationDialog,
    ],
    providers: [
        HttpService,
    ],
    bootstrap: [ConsoleComponent]
})
export class ConsoleModule { }