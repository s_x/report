﻿import {Injectable, OnInit} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Headers, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Observable';
@Injectable()
export class HttpService {
    constructor(private http: Http) {
    }
    private HTTP_TIMEOUT = 600000;
    private baseUrl = "http://reportservice.cloudapp.net:10101/api/report/websiterank/";
    getWebsiteRankDates(): Observable<any> {
        return this.get(this.baseUrl + "dates");
    }
    getWebsiteRankTop(date: string, count: number): Observable<any> {
        return this.get(this.baseUrl + "top?date=" + encodeURIComponent(date) + "&count=" + count);
    }
    private get(url: string): Observable<any> {
        var headers = new Headers();
        headers.append('Accept', 'application/json, */*');
        return this.http.get(url, { headers: headers })
            //.timeout(this.HTTP_TIMEOUT, new Error("http get time out"))       
            .map(this.extractData)
            .catch(this.handleError);
    }
    private extractData(res: Response) {
        let content = res.json();
        return content || {};
    }
    private handleError(error: any) {
        let errorMessage = (error.message) ? error.message : (error.status ? `${error.status} - ${error.statusText}` : 'Server error');
        let internalErrorMessage = "";
        try {
            internalErrorMessage = (error._body != null && error._body != undefined && error._body != "") ? JSON.parse(error._body).Message : "";
        } catch (err) {
            internalErrorMessage = "status = " + error.status + ". " + err;
        }
        return Observable.throw(errorMessage + '. ' + internalErrorMessage);
    }
}
export enum DialogType {
    Information = 1,
    Confirmation,
    Error
};
export class WebsiteStatistic {
    constructor(
        public Name: string,
        public Visits: number
    ) { }
}