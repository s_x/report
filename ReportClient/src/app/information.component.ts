﻿import {Component, ComponentFactoryResolver, ViewContainerRef} from '@angular/core';
import {DialogType} from './http.service';
@Component({
    selector: 'information-dialog',
    templateUrl: './src/app/information.component.html',
})
export class InformationDialog {
    message: string;
    title: string;
    type: DialogType;
    isVisible: boolean;
    showConfirmButtons: boolean;
    private onOk: (parameter: any) => void;
    constructor() { }
    show(type: DialogType, msg: string, callback: (parameter: any) => void = undefined) {
        this.type = type;
        switch (this.type) {
            case DialogType.Confirmation:
                this.title = "Confirmation";
                this.showConfirmButtons = true;
                break;
            case DialogType.Information:
                this.title = "Information";
                this.showConfirmButtons = false;
                break;
            case DialogType.Error:
                this.title = "Error";
                this.showConfirmButtons = false;
                break;
        }
        this.onOk = callback;
        this.message = msg;
        this.isVisible = true;
    }
    ok() {
        this.hide();
        if (this.onOk != undefined) {
            this.onOk(null);
        }
    }
    hide() {
        this.isVisible = false;
    }
}